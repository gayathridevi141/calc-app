import { useContext } from "react"
import { CalcContext } from "../context/CalcContext"

import { Textfit } from "https://cdn.skypack.dev/react-textfit@1.1.1";



const Screen = () => {
  const { calc } = useContext(CalcContext);

  return (
    <Textfit className="screen" max={70} mode="single">{calc.num ? calc.num : calc.res}</Textfit>
  )
}

export default Screen